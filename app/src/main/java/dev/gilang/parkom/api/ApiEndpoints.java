package dev.gilang.parkom.api;

public class ApiEndpoints {
    public static final String BASE_URL = "https://bristled-mineral.000webhostapp.com/parkom/api/";
    public static final String URL_MASUK = "masuk";
    public static final String URL_KELUAR = "keluar";
    public static final String URL_DATA = "getData";

}
